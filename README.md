# sharepoint-user-token Management

This source demonstrates how to use sharepoint apis in the browser and on the server and to access sharepoint as the end user rather than using a separate credential.

The main points of this are
- using an App Registration
- acquiring a token via App Registration and using [msal](https://github.com/AzureAD/microsoft-authentication-library-for-js)
- using a scope which is the sharepoint tenant `https://mytentant.sharepoint.com/.default`
- acquiring an access token in the browser and transferring to the server
- using [PnPjs](https://pnp.github.io/) library to access sharepoint
- figuring out how to use this api in commonjs without typescript
- configuring the PnPJS library with a bearer token fetch factory which takes the access token acquired in the front end

## Sharepoint API
[Sharepoint API](https://pnp.github.io/)

## App Registration

This is the [(app registration](https://portal.azure.com/#blade/Microsoft_AAD_RegisteredApps/ApplicationMenuBlade/Overview/appId/dc2428e0-defc-406e-b142-83298e273b0d/isMSAApp/) associated with this app.

The app registration is set up with permissions for Sharepoint AllSites.Read and AllSites.Write.

## Useful sample app 
This was instrumental in figuring this out

https://github.com/spblog/react-spa-msal-pnpjs


## Configuration

The following variables need to be configured into the .env file.

```
REACT_APP_TENANT_CLIENT_ID=
REACT_APP_TENANT_ID=
REACT_APP_SP_SITE_URL=
```

## Running the application

### Development 
In the project directory,
you can run `npm start` which will run the dev server which hot reload serves the front end content on port 3000.
You also separately run `npm run watch` which wil run the hot reload dev back end server which provides the apis. 

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### Configuration

### Development
Start the react front end which hot reloads changes
```
npm start
```

Start the nodejs back end which hot reloads changes
```
npm run watch
```