import React, { useState, useEffect } from 'react'
import { withRouter } from "react-router-dom";

let processes = [	
	{id: 3,title:"Sharepoint",route:"/token"},
	{id: 4,title:"PnP",route:"/pnp"}
]

function useWindowWidth() {
	const [width, setWidth] = useState(window.innerWidth);

	useEffect(() => {
		const handleResize = () => setWidth(window.innerWidth);
		window.addEventListener('resize', handleResize);
		return () => {
			window.removeEventListener('resize', handleResize);
		};
	});

	return width;
}

export default (props) => {
	return (
		<div>
			{
			processes.map(({ id, title, route }) => (
				<div>
				<a href={route}>{title}</a>
				</div>				
			))
			}
		{props.children}
		</div >
	)
}

