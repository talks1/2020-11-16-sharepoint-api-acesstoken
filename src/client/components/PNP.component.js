import React, { useEffect, useMemo, useState }  from 'react';
import { sp } from '@pnp/sp';
import "@pnp/sp/webs";
import "@pnp/sp/lists";

import { BearerTokenFetchClient } from "@pnp/nodejs";

export const PnP = (props) => {

    const [token,setToken] = useState()

    const makeRequest = async () => { 
        console.log(sp)       
        const site = await sp.site.get()
        console.log(site)
        
        const items = await sp.web.lists.getByTitle("Doc2").items.get();
        console.log(items);
    }

    const initPnPjs = async ()=> {

        
        //const accessToken = await client.getToken(["https://mytentant.sharepoint.com/.default"]);
        //console.log(accessToken)

        const accessToken = window.localStorage.getItem('AccessToken')

        console.log('Setup bearer token fetch client')
        console.log(accessToken)

        const fetchClientFactory = () => {
          console.log('Setup bearer token fetch client')
          return new BearerTokenFetchClient(accessToken);
        };
          
        sp.setup({
          sp: {
            fetchClientFactory,
            baseUrl: process.env.REACT_APP_SP_SITE_URL
          }
        });
      }

    useEffect(()=>{
        initPnPjs();

        // msalInstance.handleRedirectCallback(() => { // on success    
        //     initPnPjs();
        //   }, (authErr, accountState) => {  // on fail
        //     console.log(authErr);
    
        //     console.log(authErr.errorMessage)            
        //   });
    
        //   // if we are inside renewal callback (hash contains access token), do nothing
        //   if (msalInstance.isCallback(window.location.hash)) {
        //     console.log('hhhhhhhh')
        //     return;
        //   }
    
        //   // not logged in, perform full page redirect
        //   if (!msalInstance.getAccount()) {
        //     msalInstance.loginRedirect({});
        //     return;
        //   } else {     // logged in, set authenticated state and init pnpjs library            
        //     console.log('logged in')
        //     initPnPjs();
        //   }
    },[])

    return (<div> 
                <button onClick={makeRequest}>Make Request</button>
            </div>)
}