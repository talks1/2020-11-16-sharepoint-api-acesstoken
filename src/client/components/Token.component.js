import React, { useMemo, useState }  from 'react';
import * as msal from "@azure/msal-browser";
import {sharepointRequest} from '../models/api'

const msalConfig = {
    auth: {
        clientId: process.env.REACT_APP_TENANT_CLIENT_ID,
        authority: `https://login.microsoftonline.com/${process.env.REACT_APP_TENANT_ID}`,
        redirectUri: 'http://localhost:3000'
    }
};

const msalInstance = new msal.PublicClientApplication(msalConfig);

msalInstance.handleRedirectPromise().then(handleResponse).catch(err => {
    console.error(err);
});

function handleResponse(resp) {
    let accountId
    if (resp !== null) {
        accountId = resp.account.homeAccountId;
        //console.log(resp);
        window.localStorage.setItem('AccessToken',resp.accessToken)
        //setToken(resp.accessToken)
    } else {
        // need to call getAccount here?
        const currentAccounts = msalInstance.getAllAccounts();
        if (!currentAccounts || currentAccounts.length < 1) {
            return;
        } else if (currentAccounts.length > 1) {
            // Add choose account code here
        } else if (currentAccounts.length === 1) {
            accountId = currentAccounts[0].homeAccountId;
            //console.log(currentAccounts[0]);
            //setToken(currentAccounts[0].accessToken)
        }
    }
}


export const Token = (props) => {

    const [token,setToken] = useState()

    const localToken = window.localStorage.getItem('AccessToken')

    if(!token && localToken){
        setToken (window.localStorage.getItem('AccessToken'))
    }
    console.log(token)

    const signIn = () => {
        var loginRequest = {
            //scopes: ["user.read", "mail.send","https://technologyincubator.sharepoint.com/.default"] // optional Array<string>
            scopes: ["https://technologyincubator.sharepoint.com/.default"] // optional Array<string>
        };
        
        try {
            msalInstance.loginRedirect(loginRequest);
        } catch (err) {
            // handle error
        }
    }
    
    const makeRequest = () => {
        sharepointRequest(token)
    }

    return (<div>
                <button onClick={signIn}>Sign In</button>

                <button onClick={makeRequest}>Make Request</button>
            </div>)
}