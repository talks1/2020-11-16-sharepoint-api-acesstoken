const sp = require('@pnp/sp-commonjs')
const nodejs = require('@pnp/nodejs-commonjs')

// console.log(nodejs)

// console.log(sp.sp.setup)
//console.log(sp.sp.Site)

console.log(process.env.REACT_APP_SP_SITE_URL)
  
module.exports.setup = function (app) {

    app.get('/api/sharepoint', async (req, res) => {

        let accessToken = req.headers.authorization.replace('Bearer ','')

        const fetchClientFactory = () => {
            console.log('Setup bearer token fetch client')
            return new nodejs.BearerTokenFetchClient(accessToken);
          };
            
          sp.sp.setup({
            sp: {
              fetchClientFactory,
              baseUrl: process.env.REACT_APP_SP_SITE_URL
            }
          });
          
        const site = await sp.sp.site.get()
        console.log(site)

        const items = await sp.sp.web.lists.getByTitle("Doc2").items.get();
        console.log(items)
        
        res.status(200).send([])
    })
}
