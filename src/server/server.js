import DotEnv from 'dotenv'
import Debug from 'debug'
import assert from 'assert'
import cors from 'cors'
import express from 'express'

let DOTENV_CONFIG = process.env.ENVIRONMENT ? `.env.${process.env.ENVIRONMENT}` : '.env'
console.log(`Sourcing ${DOTENV_CONFIG}`)
DotEnv.config({path: DOTENV_CONFIG})

console.log(`Debugging with ${process.env.DEBUG}`)
const debug = Debug('app')

const app = express();
var server = require('http').createServer(app);

const routes = require('./routes.js')

assert(process.env.DIRECTORY,"environment variable DIRECTORY needs to be defined")
assert(process.env.BACKEND_URL,"environment variable BACKEND_URL needs to be defined")
assert(process.env.FRONTEND_URL,"environment variable FRONTEND_URL needs to be defined")

const config = {
  //env: process.env.ENVIRONMENT,
  backendUrl: process.env.BACKEND_URL,
  frontendUrl: process.env.FRONTEND_URL,
  port: process.env.SERVER_PORT,
  directory: process.env.DIRECTORY
}

debug(config)

app.use(cors({credentials: true,  origin: config.frontendUrl }));

function setupServer(){
  debug(`API listening on port ${config.port}!`)  
  routes.setup(app,config)
}

debug("Starting Server")
server.listen(config.port, setupServer)
